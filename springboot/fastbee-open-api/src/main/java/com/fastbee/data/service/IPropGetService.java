package com.fastbee.data.service;

/**
 * @author gsb
 * @date 2022/12/5 11:04
 */
public interface IPropGetService {

    /**
     * 属性读取
     */
    public void fetchProperty();

}
